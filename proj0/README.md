## **Author Information**

* Name: Kevin Post

* Contact address: Kpost7@uoregon.edu

## **Program description**
---

Python program that uses the configparser library to read text from an .ini file, namely "message" under the "DEFAULT" heading.
In this case, it is simply "Hello world".


## **Example usage**
```
kevin@kevin-VirtualBox:~/Documents/cis-322/cis-322/proj0$ make run
(cd hello; python3 hello.py)
"Hello world"
```
